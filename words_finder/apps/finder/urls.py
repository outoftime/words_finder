from django.conf.urls.defaults import patterns, include, url
import views

urlpatterns = patterns('',
    url(r'^text/$', views.text,
    {
        'GET': views.text_get,
        'POST': views.text_post,
    }),
    url(r'^text/get_text$', views.get_text)
)

