import threading
from functools import wraps
from django.http import Http404, HttpResponse
from django.shortcuts import render_to_response
from words_finder.settings import PROJECT_ROOT
import os, subprocess, urllib2

converter = os.path.join(PROJECT_ROOT, 'utils/pdftotext.exe')
file_input = os.path.join(PROJECT_ROOT, 'tmp/file.pdf')
file_output = os.path.join(PROJECT_ROOT, 'tmp/file.txt')

class File(object):
    def __init__(self):
        self.loading = True
        self.text = ''

    def __str__(self):
        return '{"loading":%s,"text":"%s"}' % (self.loading, self.text)

def singleton(cls):
    _instance = None
    @wraps(cls)
    def get_instance():
        if _instance is None:
            _instance = cls()
        return _instance
    return get_instance

class TimeoutError(RuntimeError):
    pass

class AsyncCall(object):
    def __init__(self, fnc, callback = None):
        self.Callable = fnc
        self.Callback = callback

    def __call__(self, *args, **kwargs):
        self.Thread = threading.Thread(target = self.run, name = self.Callable.__name__, args = args, kwargs = kwargs)
        self.Thread.start()
        return self

    def wait(self, timeout = None):
        self.Thread.join(timeout)
        if self.Thread.isAlive():
            raise TimeoutError()
        else:
            return self.Result

    def run(self, *args, **kwargs):
        self.Result = self.Callable(*args, **kwargs)
        if self.Callback:
            self.Callback(self.Result)

class AsyncMethod(object):
    def __init__(self, fnc, callback=None):
        self.Callable = fnc
        self.Callback = callback

    def __call__(self, *args, **kwargs):
        return AsyncCall(self.Callable, self.Callback)(*args, **kwargs)

def Async(fnc = None, callback = None):
    if fnc == None:
        def AddAsyncCallback(fnc):
            return AsyncMethod(fnc, callback)
        return AddAsyncCallback
    else:
        return AsyncMethod(fnc, callback)

@singleton
class LoadedFiles(object):
    _files = {}
    _lock = threading.Lock()
    _default = lambda not_used: File()

    def has_file(self, file_name):
        with self._lock:
            return self._files.has_key(file_name)

    def get(self, file_name):
        with self._lock:
            return self._files.get(file_name, self._default())

    def set(self, file_name, value):
        with self._lock:
            self._files[file_name] = value

@Async
def _load_pdf(file_url):
    if not LoadedFiles().has_file(file_url) or LoadedFiles().get(file_url).loading == False:
        LoadedFiles().set(file_url, File())
        with open(file_input, 'wb') as file:
            file.write(urllib2.urlopen(file_url).read())
        subprocess.Popen([converter, file_input, file_output]).wait()
        LoadedFiles().get(file_url).loading = False

def _load_text(file_url, file_type):
    if file_type == 'pdf':
        _load_pdf(file_url)

def get_text(request):
    text_url = request.POST.get('text_url', None)
    file = LoadedFiles().get(text_url)
    return HttpResponse(str(file))

def text_get(request, *args, **kwargs):
    return render_to_response('pdf_get.html')

def text_post(request, *args, **kwargs):
    file_url = request.POST.get('url', None)
    file_type = request.POST.get('file_type', None)

    if file_url is None or file_type is None:
        return text_get(request, *args, **kwargs)

    _load_text(file_url, file_type)
    return render_to_response('pdf_post.html', {
        'text_url': file_url
    })

def text(request, *args, **kwargs):
    get_view = kwargs.pop('GET', None)
    post_view = kwargs.pop('POST', None)

    if request.method == 'POST' and post_view is not None:
        return post_view(request, *args, **kwargs)
    if request.method == 'GET' and get_view is not None:
        return get_view(request, *args, **kwargs)
    raise Http404


