from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

#from words_finder.apps.finder.urls import urlpatterns

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'words_finder.views.home', name='home'),
    # url(r'^words_finder/', include('words_finder.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^finder/', include('words_finder.apps.finder.urls')),
)
